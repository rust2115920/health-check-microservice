# Auth Microservice

Microservice app consisting of 2 services, an authentication service and a health check service.

The auth service has three primary features:

1. Sign in
2. Sign up
3. Sign out

## Architecture

- gRPC to communicate between microservices
- Monitoring the health of microservices
- Setting up continuous integration & continuous deployment
- Using session based authentication
- Writing testable code
- Organizing code using modules
